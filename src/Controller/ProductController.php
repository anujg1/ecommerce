<?php

namespace App\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProductRepository;
use App\Service\ProductService;
use App\Entity\Product;
use App\Form\ProductType;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;





/**
 * @Route("/api/v1/product", name="product")
 */


class ProductController extends AbstractFOSRestController
{
    
    private $productRepository;

    private $productService;

    public function __construct(ProductRepository $productRepository, ProductService $productService)
    {
        $this->productRepository = $productRepository;
        $this->productService = $productService;
    }

    /**
      * @Rest\Get("/")
      *
      */
    public function getAllAction(): View
    {
        $products = $this->productService->getList();
        return $this->view($products, Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/{productId}", name="view")
     */
    public function getSingleAction(int $productId): View
    { 
        return $this->view($this->productService->getProduct($productId));
    }

    /**
     * @Rest\Post("/")
     */
    public function addAction(Request $request): View
    {   
        $product = new Product();
        $requestData =  $request->request->all();
        $form = $this->createForm(ProductType::class, $product);
        
        $form->submit($requestData);
        
        if(false == $form->isValid()) {
            return $this->view([$form->getErrors()], Response::HTTP_BAD_REQUEST);
        }

        return $this->view($this->productService->saveData($requestData), Response::HTTP_CREATED);
    }


    /**
     * @Rest\Put("/{productId}")
     */
    public function updateAction(int $productId, Request $request): View
    {  
        $product = new Product();
        $requestData =  $request->request->all();

        $form = $this->createForm(ProductType::class, $product);
        
        $form->submit($requestData);
        $result = $this->productService->updateData($productId, $requestData);

        return $this->view($result,  Response::HTTP_OK);
    }

    /**
     * @Rest\Delete("/{productId}")
     */
    public function deleteAction(int $productId): View
    {   
        return $this->view($this->productService->removeData($productId),  Response::HTTP_OK);
    }
}
