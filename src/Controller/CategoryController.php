<?php

namespace App\Controller;

use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;


/**
 * @Route("/api/v1/category", name="category")
 */

class CategoryController extends AbstractFOSRestController
{
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository )
    {
        $this->categoryRepository = $categoryRepository;
    }
    
    /**
      * @Rest\Get("/", name="index")
      *
      */
    public function getAllCategoryAction(): View
    {
        $categories = $this->categoryRepository->findAll();
        return $this->view($categories, Response::HTTP_OK);
    }

}
