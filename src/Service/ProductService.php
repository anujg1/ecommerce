<?php
namespace App\Service;

use App\Entity\Category;
use App\Entity\Product;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;

class ProductService
{
    private $productRepository;
    private $categoryRepository;
    private $entityManager;

    public function __construct(ProductRepository $productRepository, CategoryRepository $categoryRepository, EntityManagerInterface $entityManager)
    {
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
        $this->entityManager = $entityManager;
    }


    public function getProduct(int $productId)
    {
        return $this->productRepository->findOneBy(['id' => $productId]);
    }


    public function getList()
    {
        return $this->productRepository->findAll();
    }

    public function saveData(array $productData)
    {
        
        $product = new Product();
        $product->setName($productData['name']);
        $product->setSku($productData['sku']);
        $product->setPrice($productData['price']);

        $this->saveCategory($productData['category'], $product);
        
        $this->entityManager->persist($product);
        $this->entityManager->flush();

        return $product;
    }


    public function updateData(int $productId, array $productData)
    {
        $product = $this->productRepository->find($productId);
        $product->setName($productData['name']);
        $product->setSku($productData['sku']);
        $product->setPrice($productData['price']);

        $this->saveCategory($productData['category'], $product);
        
        $this->entityManager->flush();

        return $product;
    }

    public function removeData(int $productId)
    {
        $product = $this->productRepository->find($productId);
        $this->entityManager->remove($product);
        $this->entityManager->flush();

        return "Data Deleted";
    }

    public function SaveCategory(array $categoryData, $product){

        foreach($categoryData as $categoryId){
            $categoryData = $this->categoryRepository->find($categoryId);
            $product->addCategory($categoryData);
        }
    }
}